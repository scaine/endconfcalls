This little script can be placed in any directory you like. Having done so, make it executable, then use your Desktop Environment to bind it to a key (I use F8).

Then at the end of a Zoom or a Teams call, even if the window isn't focused, just tap F8 to end the call instantly.

This also works if the host of a Zoom call ends the meeting, and you only have the "Call Ended" notification.

It requires xdotool to function so on Debian/Ubuntu and their derivatives do:

`sudo apt install xdotool`

And on Arch and its derivatives, do:

`sudo pacman -S xdotool`
