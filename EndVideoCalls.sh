#!/bin/bash
currwin=`xdotool getactivewindow`

# Close Zoom during call
zoomwin=`xdotool search -all --onlyvisible --name "Zoom Meeting"`
if [ ! -z "$zoomwin" ]
then
    xdotool windowactivate $zoomwin
    xdotool sleep 0.100 key --clearmodifiers alt+q
    xdotool sleep 0.100 key --clearmodifiers Return
    xdotool windowactivate $currwin
else
    # Close Zoom after call
    endwin=`xdotool search -all --onlyvisible --name "Zoom"`
    if [ ! -z "$endwin" ]
    then
        xdotool windowactivate $endwin
        xdotool sleep 0.100 key --clearmodifiers Escape
        xdotool windowactivate $currwin
    else
        # Close Teams during call
        teamwin=`xdotool search --all --onlyvisible --name "Microsoft Teams"`
        if [ ! -z "$teamwin" ]
        then
            xdotool windowactivate $teamwin
            xdotool sleep 0.100 key --clearmodifiers ctrl+shift+h
            xdotool windowactivate $currwin
        fi
    fi
fi


